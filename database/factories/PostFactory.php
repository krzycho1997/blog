<?php

/** @var Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Foundation\Auth\User;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => User::value('id'),
        'title' => $faker->company,
        'body' => $faker->text
    ];
});
