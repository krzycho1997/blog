<?php

namespace App;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Scout\Searchable;
use ONGR\ElasticsearchDSL\Search;
use ONGR\ElasticsearchDSL\Sort\FieldSort;

class Post extends Model
{
    use Searchable;

    protected $fillable = [
        'title', 'body', 'user_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopePostsByDescWithPagination($query)
    {
        return $query
            ->orderByDesc('created_at')
            ->paginate(10);
    }

    public function scopeSearchByTitle($query, $searchTerm)
    {
        return $query->where('title', 'LIKE', $searchTerm);
    }

    public static function searchPostsByTitleWithPagination($searchTerm)
    {
        return static
            ::when($searchTerm, function ($query) use ($searchTerm) {
                return $query->searchByTitle('%' . $searchTerm . '%');
            })
            ->postsByDescWithPagination();
    }

    public static function searchPostsByTitleWithElasticsearchAndWithPagination($searchTerm): LengthAwarePaginator
    {
        return static
            ::search('title:*' . $searchTerm . '*', function($client, Search $body) {

                $sort = new FieldSort('created_at', FieldSort::DESC);
                $body->addSort($sort);

                return $client->search(['index' => 'posts', 'body' => $body->toArray()]);
            })
            ->paginate(10);
    }

    public function toSearchableArray(): array
    {
        return [
            'id'      => $this->id,
            'title'    => $this->title,
            'created_at' => $this->created_at
        ];
    }

    public function getScoutKey()
    {
        return $this->id;
    }
}
