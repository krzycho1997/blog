<?php
namespace App\Policies;

use App\User;
use App\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function view(User $user, Post $post): bool
    {
        return true;
    }

    /**
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user): bool
    {
        return $user->id > 0;
    }

    /**
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function edit(User $user, Post $post): bool
    {
        return $user->id == $post->user_id;
    }

    /**
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function update(User $user, Post $post): bool
    {
        return $user->id == $post->user_id;
    }

    /**
     *
     * @param User $user
     * @param Post $post
     * @return mixed
     */
    public function destroy(User $user, Post $post): bool
    {
        return $user->id == $post->user_id;
    }
}
