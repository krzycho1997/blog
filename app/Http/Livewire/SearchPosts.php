<?php

namespace App\Http\Livewire;

use App\Post;
use Livewire\Component;
use Livewire\WithPagination;

class SearchPosts extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $searchTerm;
    public $useElasticsearch;

    public function render()
    {
        return view('livewire.search-posts', [
            'posts' => boolval($this->useElasticsearch) && $this->searchTerm != ""
                ? Post::searchPostsByTitleWithElasticsearchAndWithPagination($this->searchTerm)
                : Post::searchPostsByTitleWithPagination($this->searchTerm)
        ]);
    }
}
