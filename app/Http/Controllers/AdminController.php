<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('admin_posts.index', [
            'posts' => Post::postsByDescWithPagination()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin_posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest $request
     * @return RedirectResponse|Redirector
     */
    public function store(PostRequest $request)
    {
        auth()->user()->posts()->create($request->all());

        return redirect()->route('postsPanel.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return ResponseFactory|Factory|Response|View
     */
    public function edit(Post $post)
    {
        return view('postsPanel.edit', [
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostRequest $request
     * @param Post $post
     *
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $post->update($request->all());

        return redirect()->route('postsPanel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return RedirectResponse|Response
     * @throws Exception
     *
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('postsPanel.index');
    }
}
