<div xmlns:wire="http://www.w3.org/1999/xhtml">

    <div class="md-form active-cyan-2 mb-3">
        <div class="form-group">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search" wire:model="searchTerm">
        </div>
        <div class="form-group">
            <input type="checkbox" value="true" wire:model="useElasticsearch">
            <label for="exampleInputPassword1">Use a elasticsearch</label>
        </div>
    </div>

    @forelse($posts as $post)
        <li>
            <a class="text-primary" >{{ $post->title }}</a>
            <a class="float-right text-primary">{{ $post->user->name }}</a>
            <p>{{ $post->body }}</p>
        </li>
    @empty
        <h1>No posts!</h1>
    @endforelse

    {{ $posts->links() }}

</div>
