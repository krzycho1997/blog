@extends('layouts.main')

@section('content')

<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h4>Latest posts</h4>
            <ul class="timeline">

                @livewire('search-posts')

            </ul>
        </div>
    </div>
</div>

@endsection

