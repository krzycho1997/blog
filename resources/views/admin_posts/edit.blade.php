@extends('adminlte::page')

@section('content')

    {!! Form::model($post, ['route' => ['admin_posts.update', $post], 'method' => 'PUT']) !!}

        @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
        @endif

        <div class="form-group">
            {!! Form::label('title', "Title:") !!}
            {!! Form::text('title', $post->title, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('body', "Body:") !!}
            {!! Form::textarea('body', $post->body, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Save', ['class'=>'btn btn-primary ']) !!}
            {!! link_to(URL::previous(), 'Cancel', ['class' => 'btn btn-default']) !!}
        </div>

    {!! Form::close() !!}
@endsection
