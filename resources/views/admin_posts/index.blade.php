@extends('adminlte::page')

@section('content')

    @if ($posts->isEmpty())
        <h1>You haven't any posts!</h1>
    @else
        <table class="table table-hover">
            <tr>
                <th>ID</th>
                <th>TITLE</th>
                <th>EDIT</th>
                <th>DELETE</th>
            </tr>
            @foreach($posts as $post)
                <tr>
                    <td>{{ $loop->iteration + (($posts->currentPage() - 1) * $posts->perPage()) }}</td>
                    <td>{{ $post->title  }}</td>
                    <td><a class="btn btn-info" href="{{ route('postsPanel.edit', $post) }}">Edit</a></td>
                    <td>
                        {{ Form::model($post, ['route' => ['postsPanel.destroy', $post], 'method' => 'DELETE']) }}
                            <button class="btn btn-danger">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </table>

        {{ $posts->links() }}

    @endif

@endsection
