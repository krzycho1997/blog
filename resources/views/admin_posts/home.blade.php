@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Profile</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Drop down profile menu!
                </div>

                @if (Auth::user()->hasRole('admin'))
                    <div class="card-body">
                        You are admin!
                        <a class="btn btn-primary" href="{{route('admin.index')}}">Go to your admin panel!</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
